﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace PerPixelCollision
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        private DebugSprite ball1, ball2;
        private Color clearColor, collisionColor;
        private readonly Rectangle gameDimensions;


        public Game1()
        {
            graphics = new GraphicsDeviceManager(this)
            {
                PreferredBackBufferWidth = 1280,
                PreferredBackBufferHeight = 720
            };
            
            Content.RootDirectory = "Content";
            gameDimensions = new Rectangle(0, 0, 1280, 720);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here
            ball1 = new DebugSprite(new Vector2(0, (graphics.GraphicsDevice.Viewport.Height / 2.0f - 120)), Color.White,70,0,gameDimensions);
            ball2 = new DebugSprite(new Vector2(graphics.GraphicsDevice.Viewport.Width, 
                (graphics.GraphicsDevice.Viewport.Height / 2.0f +115)), Color.Green, 60,MathHelper.Pi,gameDimensions);

            clearColor = Color.CornflowerBlue;
            collisionColor = Color.Red;

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here
            ball1.loadContent(Content, GraphicsDevice, "ball");
            ball2.loadContent(Content, GraphicsDevice, "ball");


        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
            ball1.Unload();
            ball2.Unload();
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here
            ball1.Update(gameTime);
            ball2.Update(gameTime);

            ball1.Collision(ball2);




            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {

            if(ball1.Collided || ball2.Collided)
            {
                GraphicsDevice.Clear(collisionColor);
            }
            else
            {
                GraphicsDevice.Clear(clearColor);
            }

            spriteBatch.Begin();
            ball1.Draw(spriteBatch, gameTime);
            ball2.Draw(spriteBatch, gameTime);
            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
