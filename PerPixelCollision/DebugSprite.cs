﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Xna.Framework.Content;

namespace PerPixelCollision
{
    class DebugSprite : Sprite
    {
        private Color rectangleColor;
        private Texture2D rectangleTexture;

        public DebugSprite(Vector2 pos, Color rectColor, float speed = 0, float angle = 0, Rectangle? bounds = null) : base(pos, speed, angle,bounds)
        {
           
            rectangleColor = rectColor;
        }

        protected override void OnContentLoaded(ContentManager content, GraphicsDevice graphicsDevice)
        {
            Color[] colors = new Color[Texture.Width * Texture.Height];
            colors[0] = rectangleColor;
            colors[Texture.Width - 1] = rectangleColor;
            colors[(Texture.Width * Texture.Height) - Texture.Width] = rectangleColor;
            colors[(Texture.Width * Texture.Height) - 1] = rectangleColor;

            rectangleTexture = new Texture2D(graphicsDevice, Texture.Width, Texture.Height);
            rectangleTexture.SetData(colors);
            base.OnContentLoaded(content, graphicsDevice);
        }

        public override void Draw(SpriteBatch spriteBatch, GameTime gameTime)
        {
            spriteBatch.Draw(rectangleTexture, null, Rectange, null, null, 0, null, Color.White);
            base.Draw(spriteBatch, gameTime);
        }

    }
}
